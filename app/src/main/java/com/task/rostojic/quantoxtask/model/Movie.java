package com.task.rostojic.quantoxtask.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
public class Movie implements Parcelable {

    @SerializedName("id")
    int id;

    @SerializedName("title")
    String title;

    @SerializedName("popularity")
    float popularity;

    @SerializedName("release_date")
    Date release_date;

    @SerializedName("poster_path")
    String poster_path;


    public int getId() {
        return id;
    }


    public int getYear() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(release_date);
        return cal.get(Calendar.YEAR);
    }

    public String getPoster() {
        return "https://image.tmdb.org/t/p/w780/" + this.poster_path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.poster_path);
        dest.writeFloat(this.popularity);
    }

    public Movie(int id, String title, float popularity, Date release_date, String poster_path) {
        this.id = id;
        this.title = title;
        this.popularity = popularity;
        this.release_date = release_date;
        this.poster_path = poster_path;
    }

    public Movie() {}

    protected Movie(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.poster_path = in.readString();
        this.popularity = in.readFloat();
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public float getPopularity() {
        return popularity;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPopularity(float popularity) {
        this.popularity = popularity;
    }


    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }
}
