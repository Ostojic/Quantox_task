package com.task.rostojic.quantoxtask.di.components;

import com.task.rostojic.quantoxtask.di.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
@Singleton
@Component(modules = NetworkModule.class)
public interface NetworkComponent {

    Retrofit retrofit();
}
