package com.task.rostojic.quantoxtask.base;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
public class BasePresenter implements Presenter {

    private CompositeSubscription mCompositeSubscription;

    @Override
    public void onCreate() {

    }

    /**
     * Call configure subscription in onResume()
     */
    @Override
    public void onResume() {
        configureSubscription();
    }

    /**
     * If mCompositeSubsription is unsubscribed create new instance
     *
     * @return mCompositeSubscription
     */
    private CompositeSubscription configureSubscription() {
        if (mCompositeSubscription == null || mCompositeSubscription.isUnsubscribed()) {
            mCompositeSubscription = new CompositeSubscription();
        }
        return mCompositeSubscription;
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {
        unSubscribeAll();
    }

    /**
     * Unsubscribe mCompositeSubscription
     */
    public void unSubscribeAll() {
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription.clear();
        }
    }

    /**
     * Asynchronously loading data
     *
     * @param observable
     * @param observer
     * @param <I>
     */
    protected <I> void subscribe(Observable<I> observable, Observer<I> observer) {
        Subscription subscription = observable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.computation())
                .subscribe(observer);
        configureSubscription().add(subscription);
    }
}
