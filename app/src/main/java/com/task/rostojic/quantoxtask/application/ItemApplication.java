package com.task.rostojic.quantoxtask.application;

import android.app.Application;
import android.content.Context;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.task.rostojic.quantoxtask.di.components.ApiComponent;
import com.task.rostojic.quantoxtask.di.components.DaggerApiComponent;
import com.task.rostojic.quantoxtask.di.components.DaggerNetworkComponent;
import com.task.rostojic.quantoxtask.di.components.NetworkComponent;
import com.task.rostojic.quantoxtask.di.module.NetworkModule;
import com.task.rostojic.quantoxtask.model.Constants;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
public class ItemApplication extends Application {

    ApiComponent mApiComponent;

    RefWatcher mRefWatcher;

    /**
     * Get RefWatcher with ItemApplication context
     *
     * @param context
     * @return
     */
    public static RefWatcher getRefWatcher(Context context) {
        ItemApplication application = (ItemApplication) context.getApplicationContext();
        return application.mRefWatcher;
    }

    @Override
    public void onCreate() {
        resolveDI();
        super.onCreate();
        mRefWatcher = LeakCanary.install(this);
    }

    /**
     * Resolve dependency injection
     * Call Network component
     */
    private void resolveDI() {
        mApiComponent = DaggerApiComponent
                .builder()
                .networkComponent(getNetworkComponent())
                .build();
    }

    /**
     * Get Network component with Network module
     *
     * @return
     */
    public NetworkComponent getNetworkComponent() {
        return DaggerNetworkComponent
                .builder()
                .networkModule(new NetworkModule(Constants.BASE_URL))
                .build();
    }

    /**
     * Return ApiComponent
     *
     * @return
     */
    public ApiComponent getApiComponent() {
        return mApiComponent;
    }

    public static ItemApplication get(Context context) {
        return (ItemApplication) context.getApplicationContext();
    }
}
