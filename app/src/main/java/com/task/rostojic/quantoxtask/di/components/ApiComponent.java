package com.task.rostojic.quantoxtask.di.components;

import com.task.rostojic.quantoxtask.di.module.ApiModule;
import com.task.rostojic.quantoxtask.di.scope.AScope;
import com.task.rostojic.quantoxtask.ui.MainActivity;

import dagger.Component;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
@AScope
@Component(modules = ApiModule.class, dependencies = NetworkComponent.class)
public interface ApiComponent {

    /**
     * Inject this Component in MainActivity
     *
     * @param activity
     */
    void inject(MainActivity activity);
}
