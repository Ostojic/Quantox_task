package com.task.rostojic.quantoxtask.service;

import com.task.rostojic.quantoxtask.model.Result;

import rx.Observable;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
public interface ItemViewInterface {

    /**
     * Inserting data completed
     */
    void Completed();

    /**
     * Call when error is happen
     *
     * @param message
     */
    void onError(String message);

    /**
     * Provide new itemResponse
     *
     * @param itemResponses
     */
    void onItems(Result itemResponses);

    /**
     * Get items using RX observable with list of ItemResponse
     *
     * @return
     */
    Observable<Result> getItems();

}
