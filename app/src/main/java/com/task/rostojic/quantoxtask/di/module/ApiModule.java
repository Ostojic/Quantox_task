package com.task.rostojic.quantoxtask.di.module;

import com.task.rostojic.quantoxtask.di.scope.AScope;
import com.task.rostojic.quantoxtask.service.ItemService;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
@Module
public class ApiModule {

    @Provides
    @AScope
    ItemService provideItemService(Retrofit retrofit) {
        return retrofit.create(ItemService.class);
    }
}
