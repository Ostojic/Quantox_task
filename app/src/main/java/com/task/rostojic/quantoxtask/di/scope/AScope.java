package com.task.rostojic.quantoxtask.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AScope {
}
