package com.task.rostojic.quantoxtask.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import static com.task.rostojic.quantoxtask.model.Constants.SQL_DB.CREATE_TABLE;
import static com.task.rostojic.quantoxtask.model.Constants.SQL_DB.DROP_TABLE;
import static com.task.rostojic.quantoxtask.model.Constants.SQL_DB.ID;
import static com.task.rostojic.quantoxtask.model.Constants.SQL_DB.POPULARITY;
import static com.task.rostojic.quantoxtask.model.Constants.SQL_DB.POSTER_PATH;
import static com.task.rostojic.quantoxtask.model.Constants.SQL_DB.RELEASE_DATE;
import static com.task.rostojic.quantoxtask.model.Constants.SQL_DB.SELECT_QUERY;
import static com.task.rostojic.quantoxtask.model.Constants.SQL_DB.TABLE_NAME;
import static com.task.rostojic.quantoxtask.model.Constants.SQL_DB.TITLE;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
public class StorageDB extends SQLiteOpenHelper {
    private static final String TAG = "StorageDB";

    public StorageDB(Context context) {
        super(context, "movies_db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }

    public void addMovie(Movie movie) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.SQL_DB.ID, movie.getId());
        values.put(Constants.SQL_DB.TITLE, movie.getTitle());
        values.put(Constants.SQL_DB.POPULARITY, movie.getPopularity());
        values.put(RELEASE_DATE, movie.getYear());
        values.put(Constants.SQL_DB.POSTER_PATH, movie.getPoster());

        db.insert(TABLE_NAME, null, values);

        db.close();
    }

    public List<Movie> getSavedCakes() {
        List<Movie> cakeList = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(SELECT_QUERY, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        Movie movie = new Movie();
                        movie.setId(cursor.getInt(cursor.getColumnIndex(ID)));
                        movie.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
                        movie.setPopularity(cursor.getFloat(cursor.getColumnIndex(POPULARITY)));
                        movie.setPoster_path(cursor.getString(cursor.getColumnIndex(POSTER_PATH)));

                        cakeList.add(movie);

                    } while (cursor.moveToNext());
                }
            }
        }
        return cakeList;
    }
}
