package com.task.rostojic.quantoxtask.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.leakcanary.RefWatcher;
import com.task.rostojic.quantoxtask.R;
import com.task.rostojic.quantoxtask.application.ItemApplication;
import com.task.rostojic.quantoxtask.base.ItemPresenter;
import com.task.rostojic.quantoxtask.model.ItemAdapter;
import com.task.rostojic.quantoxtask.model.Movie;
import com.task.rostojic.quantoxtask.model.Result;
import com.task.rostojic.quantoxtask.model.StorageDB;
import com.task.rostojic.quantoxtask.service.ItemService;
import com.task.rostojic.quantoxtask.service.ItemViewInterface;
import com.task.rostojic.quantoxtask.utils.NetworkUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
public class MainActivity extends AppCompatActivity implements ItemViewInterface {
    private static final String TAG = "MainDebug";

    @Inject
    ItemService mService;

    @BindView(R.id.recylerView)
    RecyclerView mRecycler;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @BindView(R.id.errorDisplay)
    TextView mErrorDisplay;

    @BindView(R.id.button_search)
    ImageButton mButtonSearch;

    @BindView(R.id.edit_text_movie)
    EditText mEditTextMovie;

    private ItemAdapter mAdapter;

    private ItemPresenter mPresenter;

    private StorageDB mStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inject MainActivity
        ((ItemApplication) getApplication())
                .getApiComponent()
                .inject(MainActivity.this);

        ButterKnife.bind(this);
        setViews();
        setUpSearch();

        mPresenter = new ItemPresenter(MainActivity.this);
        mPresenter.onCreate();

        mStorage = new StorageDB(this);

        loadMoviesSources();
    }

    private void setUpSearch() {
        mButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMovies(mEditTextMovie.getText().toString());
            }
        });

        setUpEditTextSearch();
    }

    private void setUpEditTextSearch() {
        mEditTextMovie.addTextChangedListener(mHideShowButtonTextWatcher);
        mEditTextMovie.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String movie = mEditTextMovie.getText().toString();
                    if (movie.length() > 0) loadMovies(movie);
                    return true;
                }
                return false;
            }
        });
    }

    private void loadMovies(String movie) {
        mProgressBar.setVisibility(View.VISIBLE);
        mRecycler.setVisibility(View.GONE);
        mService.getSearchedMovies(movie)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Result>() {
                    @Override
                    public void onCompleted() {
                        mProgressBar.setVisibility(View.GONE);
                        if (mRecycler.getAdapter().getItemCount() > 0) {
                            mRecycler.requestFocus();
                            hideSoftKeyboard();
                            Log.d(TAG, "onCompleted: da li ovde udje");
                            mRecycler.setVisibility(View.VISIBLE);
                        } else {
                            mErrorDisplay.setText(R.string.empty_movies);
                            mErrorDisplay.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mProgressBar.setVisibility(View.GONE);
                        mErrorDisplay.setText(R.string.error_warning);
                        mErrorDisplay.setVisibility(View.VISIBLE);
                        Log.d(TAG, "onError: ovde udje sigurno: " + e.getMessage());
                    }

                    @Override
                    public void onNext(Result result) {
                        ItemAdapter adapter =
                                (ItemAdapter) mRecycler.getAdapter();
                        Log.d(TAG, "onNext: da li ovde udje: " + result.results.get(1).getTitle());
                        adapter.addItems(result);
                        adapter.notifyDataSetChanged();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = ItemApplication.getRefWatcher(this);
        refWatcher.watch(this);
    }

    /**
     * Initialize RecyclerView view
     * Assign ItemAdapter to RecyclerView
     */
    private void setViews() {
        GridLayoutManager LayoutManager = new GridLayoutManager(this, 2);
        if (getResources().getConfiguration().orientation == 2) {
            LayoutManager.setSpanCount(4);
        }
        mRecycler.setLayoutManager(LayoutManager);
        mRecycler.setItemAnimator(new DefaultItemAnimator());
        mRecycler.setHasFixedSize(true);
        mRecycler.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        mAdapter = new ItemAdapter(getLayoutInflater(), MainActivity.this, this);
        mRecycler.setAdapter(mAdapter);
    }

    /**
     * Fetch data when onResume() is called
     */
    @Override
    protected void onPostResume() {
        super.onPostResume();
        mPresenter.onResume();
        mPresenter.fetchItems();
    }

    @Override
    public void Completed() {

    }

    @Override
    public void onError(String message) {
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onError: " + message);
        this.displayError(message);
    }

    @Override
    public void onItems(Result itemResponses) {
        mAdapter.addItems(itemResponses);
    }


    @Override
    public Observable<Result> getItems() {
        return mService.getItemsAllMovies();
    }

    public void displayList() {
        mRecycler.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        mErrorDisplay.setVisibility(View.GONE);
    }

    public void displayError(String errorText) {
        mRecycler.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
        mErrorDisplay.setVisibility(View.VISIBLE);
        mErrorDisplay.setText(errorText);
    }

    private void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEditTextMovie.getWindowToken(), 0);
    }

    private TextWatcher mHideShowButtonTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            mButtonSearch.setVisibility(charSequence.length() > 0 ? View.VISIBLE : View.GONE);
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    public void getMoviesFromDatabase() {
        List<Movie> movies = mStorage.getSavedCakes();
        mAdapter.addMovies(movies, true);
    }

    private void loadMoviesSources() {
        if (NetworkUtils.isNetAvailable(this)) {
            mPresenter.fetchItems();
            Log.d(TAG, "loadMovies: network available");
        } else {
            getMoviesFromDatabase();
            Log.d(TAG, "loadMovies: no network connection");
        }
    }
}
