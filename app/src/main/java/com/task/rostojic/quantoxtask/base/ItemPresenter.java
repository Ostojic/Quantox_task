package com.task.rostojic.quantoxtask.base;

import com.task.rostojic.quantoxtask.model.Result;
import com.task.rostojic.quantoxtask.service.ItemViewInterface;

import rx.Observer;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
public class ItemPresenter extends BasePresenter implements Observer<Result> {

    private ItemViewInterface mViewInterface;

    public ItemPresenter(ItemViewInterface viewInterface) {
        this.mViewInterface = viewInterface;
    }

    @Override
    public void onCompleted() {
        mViewInterface.Completed();
    }

    @Override
    public void onError(Throwable e) {
        mViewInterface.onError(e.getMessage());
    }

    @Override
    public void onNext(Result itemResponses) {
        mViewInterface.onItems(itemResponses);
    }

    /**
     * Subscribe to data from mViewInterface.getItems()
     */
    public void fetchItems() {
        unSubscribeAll();
        subscribe(mViewInterface.getItems(), ItemPresenter.this);
    }
}
