package com.task.rostojic.quantoxtask.model;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
public class Constants {

    /**
     * Base url which will be provided to Retrofit to get data
     */
    public static final String BASE_URL = "https://api.themoviedb.org";

    public class SQL_DB {

        public static final String ID = "id";
        public static final String TITLE = "title";
        public static final String POPULARITY = "popularity";
        public static final String RELEASE_DATE = "release_date";
        public static final String POSTER_PATH = "poster_path";
        public static final String TABLE_NAME = "movies";

        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
        public static final String SELECT_QUERY = "SELECT * FROM " + TABLE_NAME;

        public static final String CREATE_TABLE = "create table " + TABLE_NAME + "(" +
                ID + " integer primary key autoincrement not null," +
                TITLE + " text not null," +
                POPULARITY + " real not null," +
                RELEASE_DATE + " integer not null," +
                POSTER_PATH + " text not null)";
    }
}
