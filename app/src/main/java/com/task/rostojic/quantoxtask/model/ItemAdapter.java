package com.task.rostojic.quantoxtask.model;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.task.rostojic.quantoxtask.R;
import com.task.rostojic.quantoxtask.ui.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.MyHolder> {

    private static final String TAG = "ItemAdapterDebug";

    public final LayoutInflater mInflater;
    private ArrayList<Movie> movieList;
    private List<Movie> mMovies;

    private Context mContext;
    private MainActivity mActivity;
    private StorageDB mStorageDB;
    private boolean noNetwork = false;

    /**
     * ItemAdapter constructor
     *
     * @param inflater
     * @param context
     */
    public ItemAdapter(LayoutInflater inflater, Context context, MainActivity activity) {
        mInflater = inflater;
        movieList = new ArrayList<>();
        mContext = context;
        mActivity = activity;
        mStorageDB = new StorageDB(activity);
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_item, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, final int position) {
        if (!noNetwork) {
            Log.d(TAG, "onBindViewHolder: setting from network");
            final Movie movie = movieList.get(position);
            holder.itemTitle.setText(movie.title);
            holder.itemYear.setText("" + movie.getYear());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.themoviedb.org/movie/" + movieList.get(position).id + "?language=en-US"));
                    mContext.startActivity(intent);
                }
            });

            Glide.with(holder.itemPoster.getContext())
                    .load(movie.getPoster())
                    .into(holder.itemPoster);

        } else {
            Log.d(TAG, "onBindViewHolder: setting from database");
            final Movie movie = mMovies.get(position);
            holder.itemTitle.setText(movie.title);
            holder.itemYear.setText("" + movie.getYear());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.themoviedb.org/movie/" + movieList.get(position).id + "?language=en-US"));
                    mContext.startActivity(intent);
                }
            });

            Glide.with(holder.itemPoster.getContext())
                    .load(movie.getPoster())
                    .into(holder.itemPoster);

            mStorageDB.addMovie(movie);
        }


    }

    /**
     * Add new itemResponse in adapter
     *
     * @param itemResponses
     */
    public void addItems(Result itemResponses) {
        Log.d(TAG, "addItems: " + itemResponses.results);
        movieList = itemResponses.results;
        notifyDataSetChanged();
        mActivity.displayList();
        noNetwork = false;
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public void addMovies(List<Movie> movies, boolean noNetwork) {
        mMovies = movies;
        notifyDataSetChanged();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        ImageView itemPoster;
        TextView itemTitle;
        TextView itemYear;

        public MyHolder(View itemView) {
            super(itemView);
            itemPoster = (ImageView) itemView.findViewById(R.id.itemPoster);
            itemTitle = (TextView) itemView.findViewById(R.id.itemTitle);
            itemYear = (TextView) itemView.findViewById(R.id.itemYear);
        }
    }
}
