package com.task.rostojic.quantoxtask.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
@Module
public class NetworkModule {

    private String mBaseUrl;

    /**
     * Initialize mBaseURL
     *
     * @param baseUrl
     */
    public NetworkModule(String baseUrl) {
        mBaseUrl = baseUrl;
    }

    /**
     * Provide RxJavaCallAdapterFactory
     *
     * @return
     */
    @Provides
    @Singleton
    RxJavaCallAdapterFactory provideRxJavaCallAdapterFactory() {
        return RxJavaCallAdapterFactory.create();
    }

    /**
     * Provide GsonConverterFactory
     *
     * @return
     */
    @Provides
    @Singleton
    GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    /**
     * Provide Retrofit based on mBaseURL
     *
     * @param gsonConverterFactory
     * @param rxJavaCallAdapterFactory
     * @return
     */
    @Provides
    @Singleton
    Retrofit provideRetrofit(GsonConverterFactory gsonConverterFactory, RxJavaCallAdapterFactory rxJavaCallAdapterFactory) {
        return new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .build();
    }
}
