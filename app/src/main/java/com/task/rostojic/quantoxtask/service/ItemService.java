package com.task.rostojic.quantoxtask.service;

import com.task.rostojic.quantoxtask.model.Result;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
public interface ItemService {

    /**
     * Retrofit interface with GET method for accessing to endpoint
     *
     * @return
     */

    int VERSION = 3;
    String API_KEY = "c22d755514350d9836b3f9b173b3d763";

    @GET("/" + VERSION + "/movie/popular?api_key=" + API_KEY + "&language=en-US")
    Observable<Result> getItemsAllMovies();

    @GET("/3/search/movie")
    Observable<Result> getSearchedMovies(@Query("query") String query);
}
