package com.task.rostojic.quantoxtask.base;

/**
 * Created by Radivoje Ostojic on 7.7.18..
 */
public interface Presenter {

    void onCreate();

    void onResume();

    void onPause();

    void onDestroy();
}
